# About this fork

This fork disables the filter of black-listed content used in the official [LBRY Desktop](https://github.com/lbryio/lbry-desktop) client by changing a few lines of code. Specifically, by applying

```bash
sed -ri 's|(app\.store\.dispatch\(doBlackListedOutpointsSubscribe\(\)\);)|// \1|g' ui/index.jsx
sed -ri 's|(app\.store\.dispatch\(doFilteredOutpointsSubscribe\(\)\);)|// \1|g' ui/index.jsx
```

in the [build process](https://github.com/paveloom-f/lbry-desktop/actions).

It is a proof-of-concept piece of software, and users running it may put themselves at legal risk.

The releases for Windows, Linux, and macOS are available on the [Releases](https://github.com/paveloom-f/lbry-desktop/releases/) page.

Git mirrors:
- [Codeberg](https://codeberg.org/paveloom-f/lbry-desktop)
- [GitHub](https://github.com/paveloom-f/lbry-desktop)
- [GitLab](https://gitlab.com/paveloom-g/forks/lbry-desktop)

See also:
- [LBRY Android Fork](https://github.com/paveloom-f/lbry-android)

Learn more about:
- [DMCA policy](https://lbry.com/faq/dmca)
- [Content policy](https://lbry.com/faq/content)
- [Current DMCA takedowns](https://github.com/lbryio/dmca)
